#!/bin/awk
#Program:Write an awk script for
#1. Number of application users
#2. Total elapsed time for each user
BEGIN{
	i=0
	flag=0
	count=0
	num=0
	k=0
}
{
	a[$2]=a[$2]+$3
	count++	
}
END{	
	print "The total elapsed time for:"
	for (x in a){
		printf "%s is %s\n",x,a[x]
		num++
	}	
	print "The total number of application users:"num
}
