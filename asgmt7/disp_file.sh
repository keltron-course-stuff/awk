#Program:Shell script to print contains of file from given line number to next #given number of line. (Hint : use awk in shell script ) 
#!/bin/bash
arg=$#
if [ ! $arg -eq 3 ]
then
	echo "Invalid number of arguments."
else
	path=$1
	if [ -e $path -a -f $path ]
	then
		beg_num=$2
		end_num=$3
		awk '
		{
	 	if ( NR >'$beg_num' && NR <'$end_num' ){
			print $0	
		}
		}
		' $path
	else
		echo "File does not exist."
	fi
fi
