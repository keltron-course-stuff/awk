#Program: 
#a. Find the total number of students enrolled for the course B.Tech
#b. Find the number of students from Kannur
#c. Find the average marks for the B.Tech CS students
#d. Find the average marks for B.Tech CS students , gender wise
#e. Find the student name and admission number of the student who has got highest mark among B.Tech , ECE students

BEGIN{
	bt=0
	ka=0
	tot_marks=0
	cs_count=0
	male=0
	female=0
	tot_male=0
	tot_female=0
	max_mark=0
}
{
 if($3=="B.Tech")
  bt++
 if($5=="Kannur")
  ka++
 if($4=="CS"){
 	tot_marks=tot_marks+$6
 	cs_count++
 	if($7=="M"){
 		male++
		tot_male=tot_male+$6
	}
	else if($7=="F"){
		female++
		tot_female=tot_female+$6
	}
 }
 if($4=="ECE"){
	if($6>max_mark){
		max_mark=$6
		max_adm_num=$1
		max_name=$2
	}
 }
}
END{
	print "Number of students enrolled for B.Tech:"bt
        print "Number of students coming from Kannur:"ka
	avg=tot_marks/cs_count
	avg_male=tot_male/male
	avg_female=tot_female/female
	print "Average marks of CS students is:"avg
	print "The average marks of male students in CS:"avg_male
	print "The average marks of female students in CS:"avg_female
	print "The B.Tech ECE student with maximum marks is "max_name", his admission number is:"max_adm_num
}
