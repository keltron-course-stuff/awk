#Program:
#a) List the linux users in your machine with their home directories
#b) List only the linux users who have their home directories in /home
#How to run: awk -F : -f linux.awk /etc/passwd 
BEGIN{	
	i=0
	print "The Linux users in this system and their home directories are:"
}
{
 print "User:"$1"\tHome Directory:"$7
}
/\/home\/*/{
 a[i]=$1
 i=i+1
}
END{
 print "The linux users who have their home in /Home directory are:"
 for(j=0;j<i;j++)
 	print a[j]
}
