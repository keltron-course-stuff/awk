#Program:
#Compute the average score for every person in the list, the average score for each test, and
#the average score for each team. If a score is negative, that means the person missed the test, and the score must not become
#part of the average.
#N.B:In the list by name, the names must be left-justified in a field of size 10 (hint: %-10s in printf), and the averages must be seven characters
#wide with two digits to the right of the decimal point (%7.2f)
#How to run: awk -F , -f avg.awk list.txt

BEGIN{
	count1=0
	count2=0
	count3=0
	test_tot_1=0
	test_tot_2=0
	test_tot_3=0
	tot_red=0
	red_cnt=0
	tot_green=0
        green_cnt=0
	tot_blue=0
        blue_cnt=0
	seperator="-------------------------------"
	i=0
}
{
 tmp=0
 sub_cnt=0
 if($3>0){
	test_tot_1=test_tot_1+$3
	count1++
	tmp=tmp+$3
	sub_cnt++
 }
 if($4>0){
        test_tot_2=test_tot_2+$4
	count2++
	tmp=tmp+$4
	sub_cnt++
 }
 if($5>0){
        test_tot_3=test_tot_3+$5
	count3++
	tmp=tmp+$5
	sub_cnt++
 }
 avg_person=tmp/sub_cnt
 a[i]=avg_person
 b[i]=$1
 i++                            #To get number of persons
 if($2=="Red"){
	if($3>0){
		tot_red=tot_red+$3
		red_cnt++
	}
	if($4>0){
		tot_red=tot_red+$4
		red_cnt++
	}
	if($5>0){
		tot_red=tot_red+$5
		red_cnt++
	}
 }
 if($2=="Green"){
        if($3>0){
                tot_green=tot_green+$3
                green_cnt++
        }
        if($4>0){
                tot_green=tot_green+$4
                green_cnt++
        }
        if($5>0){
                tot_green=tot_green+$5
                green_cnt++
        }
 }
 if($2=="Blue"){
        if($3>0){
                tot_blue=tot_blue+$3
                blue_cnt++
        }
        if($4>0){
                tot_blue=tot_blue+$4
                blue_cnt++
        }
        if($5>0){
                tot_blue=tot_blue+$5
                blue_cnt++
        }
 }
}
END{
	avg_1=test_tot_1/count1
	avg_2=test_tot_2/count2
	avg_3=test_tot_3/count3
	avg_red=tot_red/red_cnt
	avg_green=tot_green/green_cnt
	avg_blue=tot_blue/blue_cnt
	printf "%-10s","Name"
	printf "%s","Average\n"
	printf "%-10s","----"
        printf "%s","-------\n"

	for(j=0;j<i;j++){
		printf "%-10s",b[j]
		printf "%7.2f\n",a[j]
	}
	printf "%s\n",seperator
	printf "Average for test 1:%7.2f",avg_1
	printf "\nAverage for test 2:%7.2f",avg_2
	printf "\nAverage for test 3:%7.2f\n",avg_3
	printf "%s\n",seperator
        printf "Average for Red team:%7.2f\n",avg_red
	printf "Average for Green team:%7.2f\n",avg_green
	printf "Average for Blue team:%7.2f\n",avg_blue
}
