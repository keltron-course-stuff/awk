#/bin/awk
#Program:Write an awk script to find the average salary of each department
BEGIN{

}
{
	a[$2]=a[$2]+$3
	b[$2]=b[$2]+1
	if($3>c[$2,2]){
		c[$2,1]=$1
		c[$2,2]=$3
	}
}
END{
	for (x in a){
		printf "\nThe average salary for %s department is %d\n",x,a[x]/b[x]
		printf "Maximum salary in %s is %d, obtained by %s.\n",x,c[x,2],c[x,1]
	}	
}
